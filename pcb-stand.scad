// Circuit board stand.  For small boards.
// You'll need two unless the board is TRULY tiny.

board_thickness = 1.6;
lip_height      = 2;
holder_width    = 5;
board_angle     = 10;

foot_width      = 10;
foot_depth     = 30;
foot_thickness  = 0.4*3;

union() {
    // Foot
    translate([0,-(foot_depth*0.4),0])
        cube([foot_width, foot_depth, foot_thickness], center = true);
    
    // Holder
    rotate([board_angle,0,0])
        translate([0,0,lip_height + foot_thickness/2 + .1])
            difference() {
                cube([holder_width, board_thickness + (0.4 * 6), lip_height*2 + 1.6], center = true);
                translate([0,0,50])
                    cube([holder_width+2, lip_height, 100],center = true);
            }
    
}